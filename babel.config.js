/*const presets = [
    [
        '@babel/preset-env',
        {
            targets: {
                edge: '17',
                firefox: '60',
                chrome: '67',
                safari: '11.1',
            },
            useBuiltIns: 'usage',
        },
    ],
];

const plugins = [
    '@babel/plugin-proposal-class-properties',
    '@babel/plugin-proposal-object-rest-spread',
    '@babel/plugin-syntax-dynamic-import',
];
module.exports = { presets, plugins };


*/
const presets = [

    ['@babel/preset-typescript'],
    ['@babel/preset-react'],
    [
        '@babel/env',
        {
            "loose": true,
            "targets": {
                "browsers": [
                    "> 0.1%",
                    "last 3 versions",
                    "ie 10",
                    "ie 11"
                ]
            }
        }
    ]
];

const plugins = [
    ['@babel/plugin-proposal-class-properties'],
    ['@babel/plugin-proposal-object-rest-spread'],
    ['@babel/plugin-syntax-dynamic-import'],
    ["@babel/transform-runtime"]
];

module.exports = {presets, plugins};