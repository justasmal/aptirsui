import {API_URL} from "./api"
import jsonServerProvider from 'ra-data-json-server';
import {stringify} from "query-string";
import {fetchUtils} from 'react-admin';

const httpClient = (url, options = {}) => {
    const token = localStorage.getItem('auth');
    if (!options.headers) {
        options.headers = new Headers({Accept: 'application/json'});
    } else {
        options.headers = new Headers();
        options.headers.set('pragma', 'no-cache');
        options.headers.set('cache-control', 'no-cache');
    }
    options.user = {
        authenticated: true,
        token: `Bearer ${token}`
    };
    return fetchUtils.fetchJson(url, options);
}
const localDataProvider = jsonServerProvider(API_URL, httpClient);

export const dataProvider = {
    ...localDataProvider,
    getStatic: (resource) =>
        httpClient(`${API_URL}/${resource}`).then(({json}) => ({
            data: json,
        })),
    changeLinkStatus: (resource, params) =>
        httpClient(`${API_URL}/${resource}`, {
            method: 'PUT',
            body: JSON.stringify(params),
        }).then(({json}) => ({
            data: json,
        })),
    getReport: (resource, params) => {
        httpClient(`${API_URL}/${resource}/?${stringify(params)}`).then(({json}) => {
            window.open(`${API_URL}/export?token=${json.token}`, '_blank');
        })

            .catch(error => {
                throw error;
            })
    }

}