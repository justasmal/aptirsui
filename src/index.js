import * as React from 'react';
import {Admin, Resource} from 'react-admin';
import {render} from 'react-dom';
import {HashRouter as Router, Route} from 'react-router-dom';
import authProvider from './authProvider';
import i18nProvider from './i18nProvider';
import Layout from './Layout';
import users from './admin/users'
import searchterms from './clients/searchterms/searchterms';
import clients from './clients/list/clients';
import titles from './clients/titles/titles';
import verify from './verify/verify';
import AuthPage from "./Auth";
import Menu from "./layout/Menu";
import {CheckPermissionLevel} from "./util/PermissionsControl";
import Dashboard from "./Dashboard";
import reports from './reports/reports';
import {QueryParamProvider} from 'use-query-params';
import {AddNewLink} from "./verify/AddNewLink";
import {dataProvider} from "./dataProvider"

const customRoutes = (props) => {
    return [
        <Route
            exact
            path="/auth"
            component={props => <AuthPage {...props} />}
            noLayout={true}
        />,
        <Route
            exact
            path="/addlink"
            component={props => <AddNewLink {...props} />}
            noLayout={false}
        />
    ];
}

const Title = ({className}) => <span className={className}>APTIRS</span>;

render(
    <Router>
        <QueryParamProvider ReactRouterRoute={Route}>
            <Admin

                authProvider={authProvider}
                dataProvider={dataProvider}
                i18nProvider={i18nProvider}
                loginPage={AuthPage}
                title={<Title/>}
                layout={Layout}
                customRoutes={customRoutes()}
                menu={Menu}

            >
                {
                    permissions => [
                        CheckPermissionLevel.isUser(permissions) ? <Resource name="/" list={Dashboard}/> : null,
                        CheckPermissionLevel.isAdmin(permissions) ? <Resource name="admin/users" {...users} /> : null,
                        CheckPermissionLevel.isManager(permissions) ? <Resource name="reports" {...reports} /> : null,
                        CheckPermissionLevel.isManager(permissions) ? <Resource name="clients" {...clients} /> : null,
                        CheckPermissionLevel.isManager(permissions) ? <Resource name="titles" {...titles} /> : null,
                        CheckPermissionLevel.isAnalyst(permissions) ?
                            <Resource name="searchterms" {...searchterms} /> : null,
                        CheckPermissionLevel.isAnalyst(permissions) ? <Resource name="links" {...verify} /> : null,
                    ]
                }

            </Admin>
        </QueryParamProvider>
    </Router>,
    document.getElementById('root')
);