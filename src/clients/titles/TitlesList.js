import * as React from 'react';
import {
    Loading,
    Title,
    useTranslate,
    Datagrid, TextField, ListContextProvider,
    useQuery,
    Pagination, DeleteWithConfirmButton,
    CreateButton, EditButton
} from 'react-admin';
import keyBy from 'lodash/keyBy'
import {useState} from 'react';
import {Box} from "@material-ui/core";
import {ClientSelect} from "../../compoments/SelectClient";



const TitlesListInternal = (props) => {
    const translate = useTranslate();
    const [page, setPage] = useState(1);
    const perPage = 5;
    const { data, total, loading, error } = useQuery({
        type: 'getList',
        resource: props.callurl,
        payload: {
            pagination: { page, perPage },
            sort: { field: 'id', order: 'ASC' },
            filter: {},
        }
    });

    if (loading) {
        return <Loading />
    }
    if (error) {
        return <p>ERROR: {error}</p>
    }
    return (
        <ListContextProvider value={{
            data: keyBy(data, 'id'),
            ids: data.map(({ id }) => id),
            total,
            page,
            perPage,
            setPage,
            currentSort: { field: 'id', order: 'ASC' },
            basePath: "/titles",
            resource: 'titles',
            selectedIds: []
        }}>
            <Datagrid>
                <TextField label={translate("data.title")} source="name" />
                <EditButton/>
                <DeleteWithConfirmButton/>
            </Datagrid>
            <Pagination />
        </ListContextProvider >
    )
}


const TitlesList = (props) => {
    const [selectedClient, setClient] = useState("");
    const translate = useTranslate();
    const handleChange = (e) => {
        setClient(e.target.value)
    }

    return (
        <div>
            <CreateButton basePath={"/titles"} />
            <Title title={translate("menu.clients.titles")}/>
            <Box width="auto">
                {' '}
                <ClientSelect onChange={handleChange}
                              selectedClient={selectedClient}/>
                {selectedClient !== "" ?
                    <TitlesListInternal {...props}  callurl={`clienttitle/${selectedClient}`}/> : ""}
            </Box>

        </div>
    )

}

export default TitlesList;