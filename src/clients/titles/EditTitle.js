
import * as React from "react";
import {
    Edit,
    SimpleForm,
    TextInput,
    required,
} from 'react-admin';
import { useTranslate } from 'react-admin';


export const EditTitle = (props) => {
    const translate = useTranslate();
    return (
        <Edit {...props} undoable={false} >
            <SimpleForm>
                <TextInput source="name" label={translate("data.title")} validate={[required()]}/>
            </SimpleForm>
        </Edit>
    )};

export default EditTitle