import * as React from "react";
import {
    Create,
    SimpleForm,
    TextInput,
    required, SelectInput, useDataProvider,
    useTranslate
} from 'react-admin';
import {useEffect, useState} from "react";


export const CreateTitle = (props) => {
    const translate = useTranslate();

    const [clients, setClients] = useState(false);
    const dataProvider = useDataProvider();
    useEffect(() => {
        function refreshUsers() {
            dataProvider.getStatic('clientslist')
                .then(({data}) => {
                    setClients(data)
                })
                .catch(error => {
                    console.error(error);
                })
        }

        refreshUsers();
    }, []);
    return (
        <div>
            {clients.length > 0 ? (

                <Create {...props}>
                    <SimpleForm>
                        <SelectInput source="client_id" label={translate("data.client_name")} choices={clients} validate={[required()]}/>
                        <TextInput source="name" label={translate("data.title")} validate={[required()]}/>
                    </SimpleForm>
                </Create>

            ) : ""
            }
        </div>
    );
}

export default CreateTitle