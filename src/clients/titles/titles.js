import TitlesList from "./TitlesList"
import EditTitle from "./EditTitle"
import CreateTitle from "./CreateTitle"
export default {
    list: TitlesList,
    edit: EditTitle,
    create: CreateTitle
};