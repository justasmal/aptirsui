import * as React from 'react';
import {
    useDataProvider,
    Loading,
    Title,
    useTranslate,
    SaveButton, Toolbar, SimpleForm, TextInput, Edit, useNotify, useRefresh
} from 'react-admin';
import {useEffect, useState} from 'react';
import Grid from "@material-ui/core/Grid";
import {TitleSelect} from "../../compoments/SelectTitles";
import {ClientSelect} from "../../compoments/SelectClient";
import {StringParam, useQueryParam} from "use-query-params";

const PostEditToolbar = props => {
    const notify = useNotify();
    const refresh = useRefresh();
    const translate = useTranslate();
    const onSuccess = (response) => {
        notify(translate("ra.notification.updated"));
        refresh();
    };
    return (
        <Toolbar {...props} >
            <SaveButton {...props} onSuccess={onSuccess}/>;
        </Toolbar>
    );
}


const SearchTermsList = (props) => {
    const dataProvider = useDataProvider();
    const [selectedClient, setClient] = useQueryParam('client', StringParam);
    const [selectedTitle, setTitle] = useQueryParam('title', StringParam);
    const [error, setError] = useState();
    const translate = useTranslate();
    const changeClientAction = (e) => {
        setClient(e.target.value)
        setTitle("");
    }

    const changeTitleAction = (e) => {
        setTitle(e.target.value)
    }
    const SearchTermTitle = ({isEdit = false}) => {
        return (<Title
            title={isEdit === false ? translate("menu.clients.searchterms") : " " + translate("data.iseditable").toLowerCase()}/>)
    }
    return (
        <Grid container spacing={3}>
            <SearchTermTitle/>
            {' '}
            <Grid item xs={12} sm={6}>
                <ClientSelect onChange={changeClientAction}
                              selectedClient={selectedClient}/>
            </Grid>

            <Grid item xs={12} sm={6}>
                <TitleSelect onChange={changeTitleAction} selectedClient={selectedClient} selectedTitle={selectedTitle}
                             Multi={false}/>

            </Grid>

            <Grid item xs={12}>
                {typeof selectedTitle !== "undefined" && selectedTitle !== "" ? (
                    <Edit {...props} title={<SearchTermTitle isEdit={true}/>} id={selectedTitle} undoable={false}
                          toolbar={<PostEditToolbar/>}>
                        <SimpleForm>
                            <TextInput toolbar={[]} fullWidth={true}
                                       rows={10} multiline source="terms"
                                       label={translate("menu.clients.searchterms")}/>
                        </SimpleForm>
                    </Edit>
                ) : <p>{translate("ra.navigation.no_results")}</p>
                }
            </Grid>

        </Grid>
    )

}

export default SearchTermsList;