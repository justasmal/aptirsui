import ClientsList from "./clientslist"
import EditClient from "./clientsedit"
import CreateClient from "./CreateClient"
export default {
    list: ClientsList,
    edit: EditClient,
    create: CreateClient
};