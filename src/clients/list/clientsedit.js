
import * as React from "react";
import {
    Edit,
    SimpleForm,
    TextInput,
    required,
    email,
} from 'react-admin';
import { useTranslate } from 'react-admin';


export const EditClient = (props) => {
    const translate = useTranslate();
    return (
        <Edit {...props} undoable={false} >
            <SimpleForm>
                <TextInput source="name" label={translate("data.client_name")} validate={[required()]}/>
                <TextInput source="email" label={translate("data.contact_email")} validate={[email()]} />
                <TextInput source="phone" label={translate("data.contact_phone")} />
            </SimpleForm>
        </Edit>
    )};

export default EditClient