import {useMediaQuery} from '@material-ui/core';
import * as React from 'react';
import {
    Datagrid, EditButton,
    List,
    SimpleList,
    TextField,
    useTranslate
} from 'react-admin';


const ClientsList = ({permissions, ...props}) => {
    const translate = useTranslate();
    return (
        <div>
            <List
                title={translate("menu.clients.list")}
                {...props}
                bulkActionButtons={false}
                exporter={false}
            >
                {useMediaQuery(theme => theme.breakpoints.down('sm')) ? (
                    <SimpleList
                        primaryText={record => record.name}
                    />
                ) : (
                    <Datagrid hasBulkActions={false}>
                        <TextField source="id" label={translate("data.id")} sortable={false}/>
                        <TextField source="name" label={translate("data.client_name")} sortable={false}/>
                        <TextField source="phone" label={translate("data.contact_phone")} sortable={false}/>
                        <TextField source="email" label={translate("data.contact_email")} sortable={false}/>
                        <EditButton/>
                    </Datagrid>
                )}
            </List>
        </div>
    )
};

export default ClientsList;
