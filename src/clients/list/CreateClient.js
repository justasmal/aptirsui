
import * as React from "react";
import {
    Create,
    SimpleForm,
    TextInput,
    required,
    email,
} from 'react-admin';

import { useTranslate } from 'react-admin';


export const CreateClient = (props) => {
    const translate = useTranslate();

    return (
        <Create {...props}>
            <SimpleForm>
                <TextInput source="name" label={translate("data.client_name")} validate={[required()]}/>
                <TextInput source="email" label={translate("data.contact_email")} validate={[email()]} />
                <TextInput source="phone" label={translate("data.contact_phone")} />
            </SimpleForm>
        </Create>
    )};

export default CreateClient