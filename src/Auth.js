import React from 'react';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import logo from './images/aptirs.png';
import MyLoginPage from "./Login";
import MyRegisterPage from "./Register";
import {useStyles} from "./user/styles";

const AuthPage = ({permissions, ...props}) => {
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <img style={{position: "relative", left: "50%", transform: "translateX(-50%)", maxHeight: "300px"}}
                         src={logo} alt="Logo"/>
                </Grid>
                <Grid item  xs={12} sm={6}>
                    <Paper className={classes.paper}>
                        <MyLoginPage {...props}/>
                    </Paper>
                </Grid>
                <Grid item xs={12} sm={6}>
                    <Paper className={classes.paper}>
                        <MyRegisterPage {...props}/>
                    </Paper>
                </Grid>
            </Grid>
        </div>

    );
}
export default AuthPage;