import ListReports from "./ListReports"
import EditReport from "./EditReport"
import CreateReport from "./CreateReport"
export default {
    list: ListReports,
    edit: EditReport,
    create: CreateReport,
};