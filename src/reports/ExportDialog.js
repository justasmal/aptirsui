import React, {useEffect, useRef, useState} from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import { useTheme } from "@material-ui/core/styles";
import {useDataProvider, useTranslate} from "react-admin";
import Grid from '@material-ui/core/Grid';
import DayjsUtils from '@date-io/dayjs';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
} from '@material-ui/pickers';
import {InputLabel, MenuItem, Select} from "@material-ui/core";
import dayjs from "dayjs";

export default function ResponsiveDialog(props) {
    const dataProvider = useDataProvider();
    const [open, setOpen] = useState(false);
    const prevReportIdRef = useRef();
    const theme = useTheme();
    const translate = useTranslate();
    const fullScreen = useMediaQuery(theme.breakpoints.down("xs"));

    const [selectedDateFrom, setSelectedDateFrom] = React.useState(dayjs().subtract(30, 'day'));
    const [selectedDateTo, setSelectedDateTo] = React.useState(dayjs());
    const [selectedLinkStatus, setLinkStatus] = React.useState("");
    const [selectedType, setType] = React.useState("xlsx");
    useEffect(() => {
        prevReportIdRef.current = props.record.id;
    });
    const reportId = prevReportIdRef.current;
    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };

    const handleGenerate = () => {
        const queryParams = {
            from: selectedDateFrom.format('YYYY-MM-DD'),
            to: selectedDateTo.format('YYYY-MM-DD'),
            link_status: selectedLinkStatus,
            type: selectedType
            //id: reportId
        };
        dataProvider.getReport(`reports/${reportId}/export`, queryParams);
        console.log([reportId, queryParams, selectedLinkStatus]);
    }


    const handleDateFromChange = (date) => {
        setSelectedDateFrom(date);
    };

    const handleDateToChange = (date) => {
        setSelectedDateTo(date);
    };

    const handleLinkStatusChange = (e) => {
        setLinkStatus(e.target.value)
    }

    const handleTypeChange = (e) => {
        setType(e.target.value)
    }


    return (
        <div>
            <Button color="primary" onClick={handleClickOpen}>
                {translate("data.generate_report")}
            </Button>
            <Dialog
                fullScreen={fullScreen}
                open={open}
                onClose={handleClose}
                aria-labelledby="responsive-dialog-title"
            >
                <DialogTitle id="responsive-dialog-title">
                    {translate("menu.reports.generate")}
                </DialogTitle>
                <DialogContent>
                    <DialogContentText>{translate("data.export_description")}</DialogContentText>
                    <MuiPickersUtilsProvider utils={DayjsUtils}>
                        <Grid container justify="space-around">
                            <KeyboardDatePicker
                                maxDate={selectedDateTo}
                                disableToolbar
                                variant="inline"
                                format="YYYY-MM-DD"
                                margin="normal"
                                label={translate("data.date_from_export")}
                                value={selectedDateFrom}
                                onChange={handleDateFromChange}
                                style={{minWidth: "100%"}}
                                KeyboardButtonProps={{
                                    'aria-label': 'change date',
                                }}
                            />
                        </Grid>
                        <Grid container justify="space-around">
                            <KeyboardDatePicker
                                minDate={selectedDateFrom}
                                maxDate={dayjs()}
                                disableToolbar
                                variant="inline"
                                format="YYYY-MM-DD"
                                margin="normal"
                                label={translate("data.date_to_export")}
                                value={selectedDateTo}
                                onChange={handleDateToChange}
                                style={{minWidth: "100%"}}
                                KeyboardButtonProps={{
                                    'aria-label': 'change date',
                                }}
                            />
                        </Grid>
                    </MuiPickersUtilsProvider>
                    <InputLabel>{translate("data.link_status_export")}</InputLabel>
                    <Select
                        displayEmpty
                        value={selectedLinkStatus}
                        label={translate("data.link_status_export")}
                        onChange={handleLinkStatusChange}
                        style={{minWidth: "100%"}}
                    >
                        <MenuItem value={""}>{translate("data.link_status_all")}</MenuItem>
                        <MenuItem value={1}>{translate("data.link_status_pirate")}</MenuItem>
                        <MenuItem value={2}>{translate("data.link_status_non_pirate")}</MenuItem>
                    </Select>
                    <InputLabel>{translate("data.link_export_format")}</InputLabel>
                    <Select
                        displayEmpty
                        value={selectedType}
                        label={translate("data.link_export_format")}
                        onChange={handleTypeChange}
                        style={{minWidth: "100%"}}
                    >
                        <MenuItem value={"xlsx"}>{translate("data.link_export_formay_xlsx")}</MenuItem>
                        <MenuItem value={"csv"}>{translate("data.link_export_formay_csv")}</MenuItem>
                    </Select>
                </DialogContent>
                <DialogActions>
                    <Button autoFocus onClick={handleClose} color="primary">
                        {translate("data.cancel")}
                    </Button>
                    <Button onClick={handleGenerate} color="primary" autoFocus>
                        {translate("data.generate")}
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}
