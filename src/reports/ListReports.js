import * as React from 'react';
import {
    useDataProvider,
    useTranslate,
    Datagrid,
    TextField,
    List,
    ArrayField,
    SingleFieldList,
    AutocompleteInput,
    Filter,
    EditButton,
    DeleteWithConfirmButton, Loading,
} from 'react-admin';
import {useEffect, useState} from 'react';
import SimpleChipField from "../compoments/SimpleChipField";
import ResponsiveDialog from "./ExportDialog";

const ReportsFilter = ({
                           loading,
                           setLoading,
                           ...props
                       }) => {
    const dataProvider = useDataProvider();
    const translate = useTranslate();
    const [clients, setClients] = useState([]);
    const loadData = () => {
        dataProvider.getStatic('clientslist')
            .then(({data}) => {
                setClients(data)
                setLoading(false)
            })
            .catch(error => {
                setLoading(false)
            })
    }
    useEffect(() => {
        loadData();
    }, []);
    if (loading === false) {
        return (
            <Filter {...props}>
                <AutocompleteInput
                    source="client"
                    choices={clients}
                    label={translate("data.client_name")}
                    alwaysOn
                />
            </Filter>
        );
    }
    return "";

}

const ListReports = (props) => {
    const [loading, setLoading] = useState(false);
    const translate = useTranslate();
    const filter = <ReportsFilter loading={loading} setLoading={setLoading}/>
    if (loading) return <Loading/>;
    return (
        <List {...props}
              exporter={false}
              filters={filter}
              perPage={25}
              bulkActionButtons={false}
        >
            <Datagrid
                hasBulkActions={false}>
                <TextField source="client.name" label={translate("data.client_name")} sortable={false}/>
                <TextField source="name" sortable={false} label={translate("data.report_name")}/>
                <ArrayField source="fields" sortable={false} label={translate("data.fields_name")}>
                    <SingleFieldList linkType={false}>
                        <SimpleChipField/>
                    </SingleFieldList>
                </ArrayField>

                <ResponsiveDialog {...props} />
                <EditButton/>
                <DeleteWithConfirmButton/>
            </Datagrid>
        </List>
    )

}

export default ListReports;