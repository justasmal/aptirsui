import * as React from "react";
import {
    Create,
    SimpleForm,
    TextInput,
    required, SelectInput, useDataProvider,
    useTranslate, SelectArrayInput
} from 'react-admin';
import {useEffect, useState} from "react";


export const CreateReport = (props) => {
    const translate = useTranslate();

    const [clients, setClients] = useState(false);
    const [fields, setFields] = useState(false);
    const dataProvider = useDataProvider();
    useEffect(() => {
        function refreshClients() {
            dataProvider.getStatic('clientslist')
                .then(({data}) => {
                    setClients(data)
                })
                .catch(error => {
                    console.error(error);
                })
        }

        function refreshFields() {
            dataProvider.getStatic('reportfields')
                .then(({data}) => {
                    setFields(data)
                })
                .catch(error => {
                    console.error(error);
                })
        }

        refreshClients();
        refreshFields();
    }, []);
    return (
        <div>
            {clients.length > 0 && fields.length > 0 ? (

                <Create {...props}>
                    <SimpleForm>
                        <SelectInput source="client_id" label={translate("data.client_name")} choices={clients} validate={[required()]}/>
                        <TextInput source="name" label={translate("data.report_name")} validate={[required()]}/>
                        <SelectArrayInput source="fields" label={translate("data.fields_name")} choices={fields}
                                          validate={[required()]}> </SelectArrayInput>
                    </SimpleForm>
                </Create>

            ) : ""
            }
        </div>
    );
}

export default CreateReport