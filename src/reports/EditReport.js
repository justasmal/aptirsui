import * as React from "react";
import {
    SimpleForm,
    TextInput,
    required, SelectInput, useDataProvider,
    useTranslate, SelectArrayInput, Edit, Loading
} from 'react-admin';
import {useEffect, useState} from "react";


export const EditReport = (props) => {
    const translate = useTranslate();

    const [clients, setClients] = useState("");
    const [fields, setFields] = useState([]);
    const dataProvider = useDataProvider();
    useEffect(() => {
        function refreshClients() {
            dataProvider.getStatic('clientslist')
                .then(({data}) => {
                    setClients(data)
                })
                .catch(error => {
                    console.error(error);
                })
        }

        function refreshFields() {
            dataProvider.getStatic('reportfields')
                .then(({data}) => {
                    setFields(data)
                })
                .catch(error => {
                    console.error(error);
                })
        }

        refreshClients();
        refreshFields();
    }, []);
    return (
        <div>
            {clients.length > 0 && fields.length > 0 ? (

                <Edit {...props} undoable={false}>
                    <SimpleForm>
                        <SelectInput disabled source="client_id" label={translate("data.client_name")} choices={clients}
                                     validate={[required()]}/>
                        <TextInput source="name" label={translate("data.report_name")} validate={[required()]}/>
                        <SelectArrayInput source="fields" label={translate("data.fields_name")} choices={fields}
                                          optionText="name" optionValue="id"
                                            validate={[required()]}> </SelectArrayInput>
                    </SimpleForm>
                </Edit>

            ) : <Loading/>
            }
        </div>
    );
}

export default EditReport