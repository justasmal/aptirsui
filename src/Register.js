import React, {useCallback, useEffect} from 'react';
import {
    useCheckAuth,
    useNotify,
    useTranslate,
    useSafeSetState,
    Notification,
    useAuthProvider,
} from 'react-admin';
import Grid from '@material-ui/core/Grid';
import {
    CardActions,
    CircularProgress,
} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import {Field, Form, useForm} from "react-final-form";
import {useHistory, useLocation} from 'react-router-dom';
import Typography from "@material-ui/core/Typography";
import {useStyles, Input} from './user/styles'

const useRegister = () => {
    const authProvider = useAuthProvider();
    const location = useLocation();
    const locationState = location.state;
    const history = useHistory();
    const nextPathName = locationState && locationState.nextPathname;

    const register = useCallback(
        (params, _) =>
            authProvider.register(params).then(ret => {
                return ret;
            }),
        [authProvider, history, nextPathName]
    );

    const loginWithoutProvider = useCallback(
        (_, __) => {
            history.push("/");
            return Promise.resolve();
        },
        [history]
    );
    return authProvider ? register : loginWithoutProvider;
};


const MyRegisterPage = props => {
    const {redirectTo} = props;
    const [loading, setLoading] = useSafeSetState(false);
    const register = useRegister();
    const translate = useTranslate();
    const notify = useNotify();
    const checkAuth = useCheckAuth();
    const history = useHistory();
    const classes = useStyles(props);
    const location = useLocation();
    useEffect(() => {
        checkAuth({}, false)
            .then(() => {
                // already authenticated, redirect to the home page
                if (location.pathname !== "/" && location.pathname !== "/login") {

                    console.log(location.pathname)
                    history.push('/');
                }
            })
            .catch((err) => {
                console.log(err)
            });
    }, [checkAuth]);

    const validate = (values) => {
        const errors = {name: undefined, email: undefined, password: undefined, confirm_password: undefined};
        if (!values.name) {
            errors.name = translate('auth.validation_required');
        }

        if (!values.email) {
            errors.email = translate('auth.validation_required');
        }
        if (!values.password) {
            errors.password = translate('auth.validation_required');
        }

        if (!values.confirm_password) {
            errors.confirm_password = translate('auth.validation_required');
        }
        if (values.password !== values.confirm_password) {
            errors.password = translate('auth.password_mismatch');
            errors.confirm_password = translate('auth.password_mismatch');
        }
        if (/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(values.email) === false) {
            errors.email = translate('auth.email_not_valid');
        }
        return errors;
    };

    const submit = async (values, form) => {
        setLoading(true);
        await register(values, redirectTo)
            .then(() => {
                Object.keys(values).forEach(key => {
                    form.change(key, undefined);
                    form.resetFieldState(key);
                });

            }).then(() => {
                setLoading(false);
                notify(
                    "auth.created"
                )
            })
            .catch(error => {
                setLoading(false);
                notify(
                    typeof error === 'string'
                        ? error
                        : typeof error === 'undefined' || !error.message
                        ? 'auth.sign_in_error'
                        : error.message,
                    'warning'
                );
            });
    };

    return (
        <div>
            <Grid container spacing={8} alignItems="flex-end">
                <Grid item md={true} sm={true} xs={true}>
                    <Typography variant="h5">
                        {translate('auth.register')}
                    </Typography>
                </Grid>
            </Grid>
            <Form
                onSubmit={submit}
                validate={validate}
                render={({handleSubmit,
                             form}) => (
                    <form onSubmit={handleSubmit}>
                        <div className={classes.margin}>
                            <Grid container spacing={8} alignItems="flex-end">
                                <Grid item md={true} sm={true} xs={true}>
                                    <Field
                                        id="name"
                                        name="name"
                                        component={Input}
                                        label={translate('auth.name')}
                                        disabled={loading}
                                    />
                                </Grid>
                            </Grid>
                            <Grid container spacing={8} alignItems="flex-end">
                                <Grid item md={true} sm={true} xs={true}>
                                    <Field
                                        id="email"
                                        name="email"
                                        component={Input}
                                        label={translate('auth.email')}
                                        disabled={loading}
                                    />
                                </Grid>
                            </Grid>
                            <Grid container spacing={8} alignItems="flex-end">
                                <Grid item md={true} sm={true} xs={true}>
                                    <Field
                                        id="password"
                                        name="password"
                                        component={Input}
                                        label={translate('auth.password')}
                                        type="password"
                                        disabled={loading}
                                    />
                                </Grid>
                            </Grid>
                            <Grid container spacing={8} alignItems="flex-end">
                                <Grid item md={true} sm={true} xs={true}>
                                    <Field
                                        id="confirm_password"
                                        name="confirm_password"
                                        component={Input}
                                        label={translate('auth.confirm_password')}
                                        type="password"
                                        disabled={loading}
                                    />
                                </Grid>
                            </Grid>

                        </div>
                        <CardActions>
                            <Button
                                variant="contained"
                                type="submit"
                                color="primary"
                                disabled={loading}
                            >
                                {loading && (
                                    <CircularProgress
                                        size={18}
                                        thickness={2}
                                    />
                                )}
                                {translate('auth.register')}
                            </Button>
                        </CardActions>
                    </form>
                )}
            />
            <Notification/>
        </div>


    );
}
export default MyRegisterPage;