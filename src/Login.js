import React, {useEffect} from 'react';
import {useCheckAuth, useNotify, useLogin, useTranslate, useSafeSetState, Notification} from 'react-admin';
import Grid from '@material-ui/core/Grid';
import {
    CardActions,
    Checkbox,
    CircularProgress,
    FormControlLabel,
} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import {Field, Form} from "react-final-form";
import {useHistory, useLocation} from 'react-router-dom';
import Typography from "@material-ui/core/Typography";
import {useStyles, Input} from './user/styles'


const MyLoginPage = props => {
    const location = useLocation();
    const [loading, setLoading] = useSafeSetState(false);
    const login = useLogin();
    const translate = useTranslate();
    const notify = useNotify();
    const checkAuth = useCheckAuth();
    const history = useHistory();
    const classes = useStyles(props);
    useEffect(() => {
        checkAuth({}, false)
            .then(() => {
                if(location.pathname !== "/") {
                    history.push('/');
                }
                // already authenticated, redirect to the home page
            })
            .catch(() => {
            });
    }, [checkAuth]);

    const validate = (values) => {
        const errors = {email: undefined, password: undefined};

        if (!values.email) {
            errors.email = translate('auth.validation_required');
        }
        if (!values.password) {
            errors.password = translate('auth.validation_required');
        }
        if (/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(values.email) === false) {
            errors.email = translate('auth.email_not_valid');
        }
        return errors;
    };

    const submit = values => {
        setLoading(true);
        login(values)
            .then(() => {
                setLoading(false);
                if(location.pathname !== "/") {
                    history.push('/');
                }
            })
            .catch(error => {
                setLoading(false);
                notify(
                    typeof error === 'string'
                        ? error
                        : typeof error === 'undefined' || !error.message
                        ? 'auth.sign_in_error'
                        : error.message,
                    'warning'
                );
            });
    };

    return (
        <div>
            <Grid container spacing={8} alignItems="flex-end">
                <Grid item md={true} sm={true} xs={true}>
                    <Typography variant="h5">
                        {translate('auth.sign_in')}
                    </Typography>
                </Grid>
            </Grid>
            <Form
                onSubmit={submit}
                validate={validate}
                render={({handleSubmit}) => (
                    <form onSubmit={handleSubmit} noValidate>
                        <div className={classes.margin}>
                            <Grid container spacing={8} alignItems="flex-end">
                                <Grid item md={true} sm={true} xs={true}>
                                    <Field
                                        id="email"
                                        name="email"
                                        component={Input}
                                        label={translate('auth.email')}
                                        disabled={loading}
                                    />
                                </Grid>
                            </Grid>
                            <Grid container spacing={8} alignItems="flex-end">
                                <Grid item md={true} sm={true} xs={true}>
                                    <Field
                                        id="password"
                                        name="password"
                                        component={Input}
                                        label={translate('auth.password')}
                                        type="password"
                                        disabled={loading}
                                        autoComplete="current-password"
                                    />
                                </Grid>
                            </Grid>
                            <Grid container alignItems="center" justify="space-between">
                                <Grid item>
                                    <FormControlLabel control={
                                        <Checkbox
                                            color="primary"
                                        />
                                    } label={translate('auth.remember_me')}/>
                                </Grid>
                                <Grid item>
                                    <Button style={{textTransform: "none"}} variant="text"
                                            color="primary">{translate('auth.forgot_password')}</Button>
                                </Grid>
                            </Grid>
                        </div>
                        <CardActions>
                            <Button
                                variant="contained"
                                type="submit"
                                color="primary"
                                disabled={loading}
                            >
                                {loading && (
                                    <CircularProgress
                                        size={18}
                                        thickness={2}
                                    />
                                )}
                                {translate('auth.sign_in')}
                            </Button>
                        </CardActions>
                    </form>


                )}
            />
            <Notification/>
        </div>
    );
}
export default MyLoginPage;