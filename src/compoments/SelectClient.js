import {useDataProvider, useTranslate} from "react-admin";
import {CircularProgress, InputLabel, MenuItem, Select, TextField} from "@material-ui/core";
import * as React from "react";
import {useEffect, useState} from "react";

export const ClientSelect = ({selectedClient, changeClientAction, onChange, ...props}) => {
    const [clients, setClients] = useState([
        {id: "", name: ""}
    ]);
    const dataProvider = useDataProvider();
    const [loading, setLoading] = useState(true)
    function fetchClient() {
        return dataProvider.getStatic('clientslist')
            .then(({data}) => {
                return data
            })
            .catch(error => {
                return null;
            })
    }
    useEffect(() => {
        let isMounted = true;
        const cList = fetchClient()
        cList.then(data => {
            if (isMounted) {
                setClients(data)
            }
        })

        setLoading(false);
        return () => { isMounted = false };

    }, []);
    const translate = useTranslate();
    const handleChange = (e) => {
        onChange(e)
    }
    if (typeof selectedClient === "undefined" || loading !== false) {
        return ""
    }
    return (
        <div>
            <InputLabel>{translate("data.client_name")}</InputLabel>

            <TextField
                select
                style={{minWidth: "100%"}}
                {...props}
                SelectProps={{
                    value: selectedClient,
                    onChange: handleChange
                }}
            >
                {clients.map((dt, i) => {
                    return (
                        <MenuItem
                            value={dt.id}
                            key={i}
                            name={dt.name}
                        >
                            {dt.name}
                        </MenuItem>
                    );
                })}

            </TextField>
        </div>

    )
}

/*
 <Select
                        value={selectedClient ?? ""}
                        onChange={handleChange}
                        style={{minWidth: "100%"}}
                        {...props}
                    >
                        {clients.map((dt, i) => {
                            return (
                                <MenuItem
                                    value={dt.id}
                                    key={i}
                                    name={dt.name}
                                >
                                    {dt.name}
                                </MenuItem>

                            );
                        })}
                    </Select>
 */