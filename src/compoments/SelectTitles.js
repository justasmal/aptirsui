import {useDataProvider, useTranslate} from "react-admin";
import {CircularProgress, InputLabel, MenuItem, Select, TextField} from "@material-ui/core";
import * as React from "react";
import {useEffect, useState} from "react";

export const TitleSelect = ({selectedClient, selectedTitle, Multi, onChange, ...props}) => {
    const translate = useTranslate();
    const dataProvider = useDataProvider();
    const [titles, setTitles] = useState([
        {id: "", name: ""}
    ]);
    const [localLoading, setLocalLoading] = useState(true)
    useEffect(() => {
        if (typeof selectedClient !== "undefined" && selectedClient !== "") {
            dataProvider.getStatic(`clienttitle/${selectedClient}`)
                .then(({data}) => {
                    setTitles(data)
                    setLocalLoading(false);
                })
                .catch(error => {
                    setLocalLoading(false);
                })
        }
    }, [selectedClient]);
    const handleChangeTitle = (e) => {
        onChange(e)
    }
    if (typeof selectedTitle === "undefined" || localLoading !== false) {
        return ""
    }
    return (
        <div>
            <InputLabel>{translate("data.title")}</InputLabel>
            <TextField
                select
                style={{minWidth: "100%"}}
                {...props}
                SelectProps={{
                    multiple: Multi,
                    value: selectedTitle,
                    onChange: handleChangeTitle
                }}
            >
                {titles.map((dt, i) => {
                    return (
                        <MenuItem
                            value={dt.id}
                            key={i}
                            name={dt.name}
                        >
                            {dt.name}
                        </MenuItem>
                    );
                })}

            </TextField>
        </div>

    )
}