import polyglotI18nProvider from 'ra-i18n-polyglot';
import lithuanianMessages from './i18n/en';

const messages = {
    //fr: () => import('./i18n/fr.js').then(messages => messages.default),
};

export default polyglotI18nProvider(locale => {
    /*if (locale === 'fr') {
        return messages[locale]();
    }*/

    // Always fallback on english
    return lithuanianMessages;
}, 'lt');
