import decodeJwt from 'jwt-decode';
import {API_URL} from "./api"

function parseToken(headers) {
    let authRes = headers.get("Authorization");
    if (authRes === null) {
        throw new Error('Failed to get response')
    }
    authRes = authRes.split(" ")[1];
    if (authRes === null) {
        throw new Error('Failed to get response')
    }
    return authRes
}

export default {
    login: ({email, password}) => {
        const request = new Request(`${API_URL}/login`, {
            method: 'POST',
            body: JSON.stringify({email, password}),
            headers: new Headers({'Content-Type': 'application/json'}),
        });
        return fetch(request)
            .then(response => {
                if (response.status < 200 || response.status >= 300) {
                    throw new Error(response.statusText);
                } else {
                    return parseToken(response.headers)
                }
            })
            .then(auth => {
                localStorage.removeItem('not_authenticated');
                const decodedToken = decodeJwt(auth);
                console.log(decodedToken);
                localStorage.setItem('role', decodedToken.role);
                localStorage.setItem('email', decodedToken.email);
                localStorage.setItem('auth', auth);
            })
            .catch((e) => {
                console.error(e);
                throw new Error('Network error')
            });
    },
    register: ({name, email, password, confirm_password}) => {
        const request = new Request(`${API_URL}/register`, {
            method: 'POST',
            body: JSON.stringify({name, email, password, confirm_password}),
            headers: new Headers({'Content-Type': 'application/json'}),
        });
        return fetch(request)
            .then(response => {
                return response.json();
            })
            .then(status => {
                if (status.success !== true) {
                    throw new Error(status.message);
                }
            })
            .catch((e) => {
                console.error(e);
                if (typeof e === "object") {
                    throw e
                } else {
                    throw new Error(e)
                }

            });
    },
    logout: () => {
        localStorage.setItem('not_authenticated', true);
        localStorage.removeItem('auth');
        localStorage.removeItem('role');
        localStorage.removeItem('login');
        localStorage.removeItem('user');
        localStorage.removeItem('avatar');
        return Promise.resolve();
    },
    checkError: ({status}) => {
        return status === 401
            ? Promise.reject({redirectTo: '/auth'})
            : Promise.resolve();
    },
    checkAuth: () => {
        const request = new Request(`${API_URL}/refresh_token`, {
            method: 'GET',
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem("auth")}`
            }),
        });
        return fetch(request)
            .then(response => {
                if (response.status < 200 || response.status >= 300) {
                    return Promise.reject();
                } else {
                    const token = parseToken(response.headers);
                    localStorage.setItem('auth', token);
                    return Promise.resolve();
                }

            }).catch((e) => {
                return Promise.reject()
            });
        /*return localStorage.getItem('not_authenticated')
            ? Promise.reject()
            : Promise.resolve();*/
    },
    getPermissions: () => {
        const role = localStorage.getItem('role');
        if (!role) {
            return Promise.reject();
        }
        return Promise.resolve(role);
    },
    getIdentity: () => {
        return {
            id: localStorage.getItem('login'),
            fullName: localStorage.getItem('user'),
            avatar: localStorage.getItem('avatar'),
        };
    },
};
