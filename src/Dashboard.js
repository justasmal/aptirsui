import * as React from 'react';
import {
    useTranslate, usePermissions, useAuthenticated
} from 'react-admin';
import {useEffect, useState} from 'react';
import {Card, CardContent, CardHeader} from "@material-ui/core";
import {CheckPermissionLevel} from "./util/PermissionsControl";

const Dashboard = (props) => {
    useAuthenticated();
    const translate = useTranslate();
    const permissions = usePermissions();
    const [isApproved, setApproval] = useState(false);
    useEffect(function(){
        setApproval(CheckPermissionLevel.isApproved(permissions.permissions))
    }, [permissions]);
    return (
        (isApproved === false) ?
            (
                <Card>
                    <CardHeader title={translate("auth.welcome")}/>
                    <CardContent>{translate("auth.non_approved_message")}</CardContent>
                </Card>
            ) : (
                <Card>
                    <CardHeader title={translate("auth.welcome")}/>
                    <CardContent>{translate("auth.approved_message")}</CardContent>
                </Card>
            )
    );

}

export default Dashboard;