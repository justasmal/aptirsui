import React, {useState} from 'react';
import {useSelector} from 'react-redux';
import {useMediaQuery} from '@material-ui/core';
import {useTranslate, MenuItemLink} from 'react-admin';
import SubMenu from './SubMenu';
import AdminIcon from '@material-ui/icons/SupervisorAccount';
import WorkIcon from '@material-ui/icons/Work';
import AssessmentIcon from '@material-ui/icons/Assessment';
import StorageIcon from '@material-ui/icons/Storage';
import TimelineIcon from '@material-ui/icons/Timeline';
import {usePermissions} from 'react-admin';
import {CheckPermissionLevel} from '../util/PermissionsControl';
import FindInPageIcon from '@material-ui/icons/FindInPage';
import Divider from "@material-ui/core/Divider";
import Typography from "@material-ui/core/Typography";
import AddBoxIcon from '@material-ui/icons/AddBox';

import logo from '../images/aptirs.png';


const Menu = ({onMenuClick, dense, logout}) => {
    const [state, setState] = useState({
        menuAdmin: false,
        menuClient: false,
        menuReports: false,
    });
    const {permissions} = usePermissions();
    const translate = useTranslate();
    const isXsmall = useMediaQuery(theme => theme.breakpoints.down('xs'));
    const open = useSelector(state => state.admin.ui.sidebarOpen);
    const email = `${translate("data.name")}: ${localStorage.getItem('email')}`;
    const role = `${translate("data.role")}: ${translate(`role.${localStorage.getItem('role')}`)}`;
    useSelector(state => state.theme);
    const handleToggle = menu => {
        setState(state => ({...state, [menu]: !state[menu]}));
    };
    return (
        <div>
            {' '}
            <img style={{
                justifyContent: "center",
                maxWidth: "100%"
            }} src={logo} alt={"logo"}/>
            <Divider/>
            {open ?
                <div>
                    <Typography variant="body2">
                        {email}
                    </Typography>
                    <Typography variant="body2">
                        {role}
                    </Typography>
                    <Divider/>
                </div> : ""
            }

            {CheckPermissionLevel.isAdmin(permissions) &&
            <SubMenu
                handleToggle={() => handleToggle('menuAdmin')}
                isOpen={state.menuAdmin}
                sidebarIsOpen={open}
                name="menu.admin.main"
                icon={<AdminIcon/>}
                dense={dense}
            >
                <MenuItemLink
                    to={`/`}
                    primaryText={translate(`menu.admin.config`, {
                        smart_count: 2,
                    })}
                    onClick={onMenuClick}
                    sidebarIsOpen={open}
                    dense={dense}
                    replace
                />
                <MenuItemLink
                    to={`/admin/users`}
                    primaryText={translate(`menu.admin.users`, {
                        smart_count: 2,
                    })}
                    onClick={onMenuClick}
                    sidebarIsOpen={open}
                    dense={dense}
                    replace
                />
            </SubMenu>
            }
            {CheckPermissionLevel.isManager(permissions) &&
            <SubMenu
                handleToggle={() => handleToggle('menuClient')}
                isOpen={state.menuClient}
                sidebarIsOpen={open}
                name="menu.clients.main"
                icon={<WorkIcon/>}
                dense={dense}
            >
                <MenuItemLink
                    to={`/clients` }
                    primaryText={translate(`menu.clients.list`, {
                        smart_count: 2,
                    })}
                    onClick={onMenuClick}
                    sidebarIsOpen={open}
                    dense={dense}
                    replace
                />
                <MenuItemLink
                    to={`/titles` }
                    primaryText={translate(`menu.clients.titles`, {
                        smart_count: 2,
                    })}
                    onClick={onMenuClick}
                    sidebarIsOpen={open}
                    dense={dense}
                    replace
                />
                <MenuItemLink
                    to={`/searchterms`}
                    primaryText={translate(`menu.clients.searchterms`, {
                        smart_count: 2,
                    })}
                    onClick={onMenuClick}
                    sidebarIsOpen={open}
                    dense={dense}
                    replace
                />
            </SubMenu>
            }
            {(!CheckPermissionLevel.isManager(permissions) && CheckPermissionLevel.isAnalyst(permissions)) &&
            <MenuItemLink
                to={`/searchterms`}
                primaryText={translate(`menu.clients.searchterms`, {
                    smart_count: 2,
                })}

                leftIcon={<FindInPageIcon/>}
                onClick={onMenuClick}
                sidebarIsOpen={open}
                dense={dense}
                replace
            />

            }
            {CheckPermissionLevel.isAdmin(permissions) &&
            <MenuItemLink
                to={`/`}
                primaryText={translate(`menu.collection.main`, {
                    smart_count: 2,
                })}
                onClick={onMenuClick}
                sidebarIsOpen={open}

                leftIcon={<StorageIcon/>}
                dense={dense}
                replace
            />
            }
            {CheckPermissionLevel.isAnalyst(permissions) &&
            <MenuItemLink
                to={`/links`}
                primaryText={translate(`menu.verification.main`, {
                    smart_count: 2,
                })}
                leftIcon={<TimelineIcon/>}
                onClick={onMenuClick}
                sidebarIsOpen={open}
                dense={dense}
                replace
            />
            }
            {CheckPermissionLevel.isManager(permissions) &&
            <MenuItemLink
                to={`/reports`}
                primaryText={translate(`menu.reports.generate`, {
                    smart_count: 2,
                })}
                onClick={onMenuClick}
                sidebarIsOpen={open}
                dense={dense}
                leftIcon={<AssessmentIcon/>}
                replace
            />
            }
            {CheckPermissionLevel.isAnalyst(permissions) &&
            <MenuItemLink
                to={`/addlink`}
                primaryText={translate(`menu.verification.add_link`, {
                    smart_count: 2,
                })}
                leftIcon={<AddBoxIcon/>}
                onClick={onMenuClick}
                sidebarIsOpen={open}
                dense={dense}
                replace
            />
            }

            {isXsmall && logout}
        </div>
    );
};

export default Menu;
