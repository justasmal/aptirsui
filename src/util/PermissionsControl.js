const menuPermissionTable = {
    ROLE_ADMIN: 1,
    ROLE_MANAGER: 2,
    ROLE_ANALYST: 3,
    ROLE_USER: 99999,
}

const isAdmin = (permission) => {
    if( permission ) {
        return menuPermissionTable[permission] <= menuPermissionTable.ROLE_ADMIN;
    }
    return false;
}

const isManager = (permission) => {
    if( permission ) {
        return menuPermissionTable[permission] <= menuPermissionTable.ROLE_MANAGER;
    }
    return false;
}

const isAnalyst = (permission) => {
    if( permission ) {
        return menuPermissionTable[permission] <= menuPermissionTable.ROLE_ANALYST;
    }
    return false;
}

const isUser = (permission) => {
    if( permission ) {
        return menuPermissionTable[permission] <= menuPermissionTable.ROLE_USER;
    }
    return false;
}

const isApproved = (permission) => {
    if( permission ) {
        return menuPermissionTable[permission] < menuPermissionTable.ROLE_USER;
    }
    return false;
}

export const CheckPermissionLevel = {
    isAdmin,
    isManager,
    isAnalyst,
    isUser,
    isApproved
}
export const getRoles = (translate) => {
    const roleArr = Object.keys(menuPermissionTable);
    let roles = [];
    for(let i=0; i<roleArr.length; i++) {
        roles.push({id: roleArr[i], name: translate(`role.${roleArr[i]}`)});
    }
    return roles;
}