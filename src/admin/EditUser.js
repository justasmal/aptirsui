
import * as React from "react";
import {
    Edit,
    SimpleForm,
    TextInput,
    SelectInput,
    required,
    email,
} from 'react-admin';
import {getRoles} from "../util/PermissionsControl";

import { useTranslate } from 'react-admin';


export const EditUser = (props) => {
    const translate = useTranslate();
    return (
    <Edit {...props} undoable={false} >
        <SimpleForm>
            <TextInput source="name" label={translate("data.name")} validate={[required()]}/>
            <TextInput source="email" label={translate("data.email")} validate={[required(), email()]} />
            <SelectInput label={translate("data.role")} validate={[required()]} source="role" choices={getRoles(translate)} />
        </SimpleForm>
    </Edit>
)};

export default EditUser