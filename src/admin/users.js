import UsersList from "./userslist"
import EditUser from "./EditUser"
export default {
    list: UsersList,
    edit: EditUser,
};