import * as React from 'react';
import {
    List,
    Datagrid,
    TextField,
    EditButton,
    DeleteWithConfirmButton,
    FilterList, FilterListItem
} from 'react-admin';
import {useTranslate} from 'react-admin';
import {useMediaQuery,  Card as MuiCard, CardContent, withStyles } from "@material-ui/core";
import TransferWithinAStation from '@material-ui/icons/TransferWithinAStation';


const Card = withStyles(theme => ({
    root: {
        [theme.breakpoints.up('sm')]: {
            order: -1, // display on the left rather than on the right of the list
            width: '15em',
            marginRight: '1em',
        },
        [theme.breakpoints.down('sm')]: {
            display: 'none',
        },
    },
}))(MuiCard);

const HasOrderedFilter = () => {

    const translate = useTranslate();
    return (<FilterList
            label='data.active_status'
            icon={<TransferWithinAStation/>}
        >
            <FilterListItem
                label={translate('data.active_users')}
                value={{
                    type: "active",
                }}
            />
            <FilterListItem
                label={translate('data.inactive_users')}
                value={{
                    type: "inactive",
                }}
            />
        </FilterList>
    )
}

const FilterSidebar = () => (
    <Card>
        <CardContent>
            <HasOrderedFilter />
        </CardContent>
    </Card>
);

const UsersList = (props) => {
    const translate = useTranslate();
    return (
        <List
        title={translate("menu.admin.users")}
        {...props}
        bulkActionButtons={false}
        exporter={false}
        aside={<FilterSidebar />}
    >
            {useMediaQuery(theme => theme.breakpoints.down('sm')) ? (
                <Datagrid rowClick="edit">
                    <TextField source="name" label={translate("data.name")} sortable={false} />
                    <TextField source="email" label={translate("data.email")} sortable={false} />
                </Datagrid>
            ) : (
                <Datagrid>
                    <TextField source="name" label={translate("data.name")} sortable={false} />
                    <TextField source="email" label={translate("data.email")} sortable={false} />
                    <TextField source="role" label={translate("data.role")} sortable={false} />
                    <EditButton/>
                    <DeleteWithConfirmButton/>
                </Datagrid>
            )}
    </List>)

}

export default UsersList;