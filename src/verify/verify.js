import * as React from 'react';
import {
    useDataProvider,
    Loading,
    Title,
    useTranslate,
} from 'react-admin';
import {useEffect, useState} from 'react';
import Grid from "@material-ui/core/Grid";
import CustomVerifyList from "./CustomVerifyList";
import {ArrayParam, StringParam, useQueryParam, withDefault} from "use-query-params";
import {TitleSelect} from "../compoments/SelectTitles";
import {ClientSelect} from "../compoments/SelectClient";

const VerifyPage = (props) => {
    const [selectedClient, setClient] = useQueryParam('client', withDefault(StringParam, ""));
    const [selectedTitles, setTitlesToVerify] = useQueryParam('titles',  withDefault(ArrayParam, []));
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(false);
    const translate = useTranslate();

    const handleChange = (e) => {
        setClient(e.target.value);
        setTitlesToVerify([]);

    }
    const handleChangeTitle = (e) => {
        setTitlesToVerify([...e.target.value.map(String)]);

    }
    if (loading) return <Loading/>;
    if (error) {
        return null;
    }
    return (
        <Grid container spacing={5}>
            <Title title={translate("menu.verification.main")}/>
            {' '}
            <Grid item xs={12} sm={6}>
                <ClientSelect selectedClient={selectedClient} onChange={handleChange} />
            </Grid>

            <Grid item xs={12} sm={6}>
                <TitleSelect onChange={handleChangeTitle} selectedClient={selectedClient} selectedTitle={selectedTitles} Multi={true}/>
            </Grid>
            <Grid item xs={12}>
                {
                    selectedTitles.length > 0 ? <CustomVerifyList {...props} selectedTitles={selectedTitles} /> : ""
                }

            </Grid>

        </Grid>
    )

}



export default {
    list: VerifyPage,
};