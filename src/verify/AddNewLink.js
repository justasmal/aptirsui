import * as React from "react";
import {
    Create,
    SimpleForm,
    TextInput,
    required, SelectInput, useDataProvider,
    useTranslate, useAuthenticated, Edit, Notification, Title, useNotify, useRefresh, useQuery, usePermissions
} from 'react-admin';
import {useState} from "react";
import Grid from "@material-ui/core/Grid";
import {ClientSelect} from "../compoments/SelectClient";
import {TitleSelect} from "../compoments/SelectTitles";
import {Field, Form} from "react-final-form";
import {Input, useStyles} from "../user/styles";
import {
    CardActions,
    CircularProgress,
    InputLabel,
} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import {OnChange} from "react-final-form-listeners";
import {CheckPermissionLevel} from "../util/PermissionsControl";
import {useHistory} from "react-router-dom";


export const AddNewLink = (props) => {
    const dataProvider = useDataProvider();
    const translate = useTranslate();
    const [selectedClient, setClient] = useState("");
    const [selectedTitle, setTitle] = useState("");
    const [loading, setLoading] = useState(false);
    const notify = useNotify();
    const history = useHistory();
    const refresh = useRefresh();
    const classes = useStyles(props);
    const {loaded, permissions} = usePermissions();
    if (loaded && !CheckPermissionLevel.isAnalyst(permissions)) {
        notify(
            translate('ra.notification.created'),
            "warning"
        );
        history.push("/");
    }
    const SelectExtendedInput = ({
                                     meta: {touched, error}, // eslint-disable-line react/prop-types
                                     input: inputProps, // eslint-disable-line react/prop-types
                                     ...props
                                 }) => {
        return (

            <TitleSelect
                selectedTitle={selectedTitle}
                selectedClient={selectedClient}
                Multi={false}
                error={!!(touched && error)}
                helperText={touched && error}
                {...inputProps}
                {...props}
                fullWidth
            />
        );
    }

    const SelectClientInput = ({
                                   meta: {touched, error}, // eslint-disable-line react/prop-types
                                   input: inputProps, // eslint-disable-line react/prop-types
                                   ...props
                               }) => {
        return (
            <ClientSelect onChange={changeClientAction}
                          selectedClient={selectedClient}
                          disabled={loading}
                          error={!!(touched && error)}
                          helperText={touched && error}
                          {...inputProps}
                          {...props}
                          fullWidth
            />
        );
    }
    const changeClientAction = (e) => {
        setClient(e)
    }
    const changeTitleAction = (e) => {
        setTitle(e)
    }

    const validate = (values) => {
        console.log(values)
        const errors = {client: undefined, title: undefined, url: undefined, linking_url: undefined};
        if (!values.client || values.client === "") {
            errors.client = translate('auth.validation_required');
        }
        if (!values.title || values.title === "") {
            errors.title = translate('auth.validation_required');
        }
        if (!values.url) {
            errors.url = translate('auth.validation_required');
        }
        if (!values.linking_url) {
            errors.linking_url = translate('auth.validation_required');
        }
        return errors;
    };

    const submit = async (values, form) => {
        setLoading(true);
        dataProvider.create('links', {data: values}).then(response => {
            refresh();
            form.reset();
            setLoading(false);
            notify(
                'ra.notification.created'
            );
        }).catch(error => {
            notify(
                typeof error === 'string'
                    ? error
                    : typeof error === 'undefined' || !error.message
                    ? 'auth.sign_in_error'
                    : error.message,
                'error'
            );
            setLoading(false);

        })/*
        const { data, loading, error } = useQuery({
            type: 'create',
            resource: 'links',
            payload: { values }
        });
        if()
        setTimeout(function () {
            refresh();
            setLoading(false);
            notify(
                "auth.created"
            )
        }, 1000)*/
        /*await register(values, redirectTo)
            .then(() => {
                Object.keys(values).forEach(key => {
                    form.change(key, undefined);
                    form.resetFieldState(key);
                });

            }).then(() => {
                setLoading(false);
                notify(
                    "auth.created"
                )
            })
            .catch(error => {
                setLoading(false);
                notify(
                    typeof error === 'string'
                        ? error
                        : typeof error === 'undefined' || !error.message
                        ? 'auth.sign_in_error'
                        : error.message,
                    'warning'
                );
            });*/
    };

    return (
        <div>
            <Title title={translate("menu.verification.add_link")}/>
            <Form
                onSubmit={submit}
                validate={validate}
                render={({handleSubmit}) => (
                    <form onSubmit={handleSubmit} noValidate>
                        <div className={classes.margin}>
                            <Grid container spacing={8} alignItems="flex-end">
                                <Grid item md={true} sm={true} xs={true}>
                                    <Field
                                        id="client"
                                        name="client"
                                        component={SelectClientInput}
                                        defaultValue={""}
                                        disabled={loading}
                                    />
                                    <OnChange name="client">
                                        {(value, previous) => {
                                            changeClientAction(value)
                                        }}
                                    </OnChange>

                                </Grid>


                            </Grid>
                            <Grid container spacing={8} alignItems="flex-end">
                                <Grid item md={true} sm={true} xs={true}>
                                    <Field
                                        id="title"
                                        name="title"
                                        component={SelectExtendedInput}
                                        defaultValue={""}
                                        disabled={loading}
                                    />
                                    <OnChange name="title">
                                        {(value, previous) => {
                                            changeTitleAction(value)
                                        }}
                                    </OnChange>

                                </Grid>
                            </Grid>
                            <Grid container spacing={8} alignItems="flex-end">
                                <Grid item md={true} sm={true} xs={true}>
                                    <InputLabel>{translate("data.url")}</InputLabel>
                                    <Field
                                        id="url"
                                        name="url"
                                        component={Input}
                                        disabled={loading}
                                    />
                                </Grid>
                            </Grid>
                            <Grid container spacing={8} alignItems="flex-end">
                                <Grid item md={true} sm={true} xs={true}>
                                    <InputLabel>{translate("data.linking_url")}</InputLabel>
                                    <Field
                                        id="linking_url"
                                        name="linking_url"
                                        component={Input}
                                        disabled={loading}
                                    />
                                </Grid>
                            </Grid>
                        </div>
                        <CardActions>
                            <Button
                                variant="contained"
                                type="submit"
                                color="primary"
                                disabled={loading}
                            >
                                {loading && (
                                    <CircularProgress
                                        size={18}
                                        thickness={2}
                                    />
                                )}
                                {translate('ra.action.add')}
                            </Button>
                        </CardActions>
                    </form>


                )}
            />
            <Notification/>
        </div>
    );
}