import * as React from 'react';
import keyBy from 'lodash/keyBy'
import {
    ListContextProvider,
    Datagrid,
    TextField, useQuery, Loading, useRefresh, useDataProvider, useNotify,
} from 'react-admin'
import {useState} from "react";
import {useTranslate} from 'react-admin';
import {Button} from "@material-ui/core";


const VerifyButton = (props) => {
    const translate = useTranslate();
    const refresh = useRefresh();
    const notify = useNotify();
    const dataProvider = useDataProvider();
    const handleEvent = (e) => {
        props.record.status = props.action;
        dataProvider.changeLinkStatus(`links`, props.record)
            .then(() => {
                notify(translate("data.link_status_changed"))
                refresh();
            })
            .catch(error => {
                notify(translate("data.link_status_change_fail"), "error")
            })
        e.stopPropagation();

    }

    return (<Button color={props.action === 1 ? "secondary" : "primary"} onClick={handleEvent}>
        {props.action === 1 ? translate("data.mark_as_pirate") : ""}
        {props.action === 2 ? translate("data.mark_as_nonpirate") : ""}
    </Button>);
}

const CustomVerifyList = (props) => {
    const translate = useTranslate();
    const [page, setPage] = useState(1);
    const perPage = 50;
    const {data, total, loading, error} = useQuery({
        type: 'getList',
        resource: props.resource,
        payload: {
            pagination: {page, perPage},
            sort: {field: 'id', order: 'ASC'},
            filter: {titles: props.selectedTitles.join(","), status: 0},
        }
    });

    if (typeof data === "undefined") {
        return <Loading/>
    }
    if (error) {
        return <p>ERROR: {error}</p>
    }
    return (
        <ListContextProvider value={{
            data: keyBy(data, 'id'),
            ids: data.map(({id}) => id),
            total,
            page,
            perPage,
            setPage,
            currentSort: {field: 'id', order: 'ASC'},
            basePath: "/links",
            resource: 'links',
            selectedIds: []
        }}>
            <Datagrid>
                <TextField label={translate("data.url")} source="url" sortable={false}  />
                <TextField label={translate("data.title_name")} source="title_info.name" sortable={false}  />
                <TextField label={translate("data.linking_url")} source="linking_url" sortable={false}  />
                <TextField label={translate("data.description")} source="description" sortable={false}  />
                <VerifyButton {...props} action={1} />
                <VerifyButton {...props} action={2} />
            </Datagrid>
        </ListContextProvider>
    )
}

export default CustomVerifyList;