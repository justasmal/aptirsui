FROM node:14-alpine as build-stage

WORKDIR /app
COPY package*.json /app/
RUN npm install
COPY ./ /app/
RUN npm run buildprod


FROM nginx:1.19-alpine
COPY --from=build-stage /app/dist/ /usr/share/nginx/html
