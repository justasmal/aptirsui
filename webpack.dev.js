const { merge } = require('webpack-merge');
const common = require('./webpack.common.js');
const path = require('path');

module.exports = merge(common, {
    mode: 'development',
    devtool: 'cheap-module-source-map',
    devServer: {
        hot: true,
        contentBase: path.join(__dirname, 'dist'),
        writeToDisk: true,
        disableHostCheck: true,
        host: '127.0.0.1',
        port: 8081,
        stats: {
            children: false,
            chunks: false,
            modules: false,
        },
    },
});