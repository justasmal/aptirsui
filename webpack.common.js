const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const fs = require('graceful-fs').gracefulify(require('fs'));

module.exports = {
    module: {
        rules: [
            {
                test: /\.[tj]s?$/,
                exclude: /node_modules/,
                use: {loader: 'babel-loader'},
            },
            {
                test: /\.[tj]sx?$/,
                exclude: /node_modules/,
                use: {loader: 'babel-loader'},
            },
            {
                test: /\.html$/,
                exclude: /node_modules/,
                use: {loader: 'html-loader'},
            },
            {
                test: /\.(png|svg|jpg|jpeg|gif)$/i,
                use: [
                    {
                        loader: 'file-loader',
                    },
                ]
            },
        ],
    },
    optimization: {
        splitChunks: {
            chunks: 'all',
            minSize: 0,
            cacheGroups: {
                react: {
                    test: /[\\/]node_modules[\\/](react|react-dom)[\\/]/,
                    name: "react"
                },
                material: {
                    test: /[\\/]node_modules[\\/](@material-ui)[\\/]/,
                    name: "material"
                },
                vendor: {
                    test: /[\\/]node_modules[\\/](!@material-ui)(!react)(!react-dom)[[\\/]/,
                    name: "vendor"
                },
            },
        },
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/index.html',
        }),
    ],
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'static/js/[name].[contenthash:8].js'
    },
};
